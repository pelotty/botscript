﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

init()
{
        global mleft := -1000
        global mright := -1000
        global mheight := -1000
        global mover := 0
        global state := 0
        global jump := true
        global wait :=false
        global manual := true
        global forcebuff := false
}
init()
Suspend



Test()
{
    AutoPassiveSkill()
}



AutoPassiveSkill()
{
    global ebufact
    global manual
    if(manual)
    {
        return
    } 
    CoordMode Pixel
    ebufact := false
    ImageSearch, sox, soy, 0, 0, A_ScreenWidth, A_ScreenHeight,*80 ebuff.png
    if (ErrorLevel == 0){
         ebufact := true
    }else
    {
         
        CoordMode Pixel
        ImageSearch, x, y, 0, 0, A_ScreenWidth, A_ScreenHeight,e.png
         if (ErrorLevel == 0){
            send {e}
         }

    }
}
SetTimer, AutoPassiveSkill, 1000



SetLeft()
{
    global mleft
    CoordMode Pixel
    ImageSearch, mleft, soy, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
    if(ErrorLevel == 0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}

SetRight()
{
    global mright
    CoordMode Pixel
    ImageSearch, mright, soy, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
    if(ErrorLevel == 0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}

SetHeight()
{
    global mheight
    CoordMode Pixel
    ImageSearch, sox, mheight, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
    if(ErrorLevel == 0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}

mainmove()
{

    global mleft
    global mright 
    global mheight
    global mover
    global state
    global wait
    global manual
    GetKeyState, tabstate, Tab
    if (tabstate = "D")
        keybuff()
    if(manual)
    {
        return
    } 
    if(mleft >= 0 and mright >=0)
    {
        ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
        if(ErrorLevel == 0)
        {
            if(px <= mleft and state != -1)
            {
                send {left UP}
                sleep 50
                send {right Down}
                state := -1
            }Else
            if(px >= mright and state != 1)
            {
                send {right UP}
                sleep 50
                send {left Down}
                state := 1
            }

        }
    }
}
SetTimer, mainmove, 30



Attack()
{
    global jump
    global wait
    global mover
    global state    
    global mheight
    global ebufact
    global manual
    if(manual)
    {
        return
    } 

    if(wait==false)
    {

        ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
        if(ErrorLevel == 0)
        {
            if(state == mover)
            {
                if(py > mheight)
                {
                    wait := true
                    sleep 40
                    send {x down}
                    sleep 40
                    send {x up}
                    sleep 40
                    send {Del down}
                    sleep 40
                    send {Del up}
                    wait := false
                    return
                }
            }
        }
        if(jump)
        {
            send r
        }
        autoAttack(false)
    }
}
SetTimer,Attack,100

Rune()
{
    ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight,*50 lock.png
    if(ErrorLevel==0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}
SetTimer,Rune,3000



bot()
{
    ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight,*50 botcheck.png
    if(ErrorLevel==0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}
SetTimer,bot,3000



NormalBuff()
{

    global wait
    global manual
    if(manual)
    {
        return
    }  
    if(wait == false)
    {
        wait := true
        send {1}
        sleep 500
        send {Ctrl}
        wait := false
    }
}
SetTimer,NormalBuff,30000
NormalBuff()


keybuff()
{
    global wait
    global forcebuff
    forcebuff := true
    wait := false
    buff()
}

EventSkill()
{
    global manual
    if(manual)
    {
        return
    }     
    send {c Down}
    sleep 50
    send {c Up}
}
SetTimer,EventSkill,15000



buff()
{
    global forcebuff
    global manual
    if(manual and forcebuff == false )
    {
        return
    } 
    forcebuff := false
    CoordMode Pixel
    ImageSearch, ox1, oy1, 0, 0, A_ScreenWidth, A_ScreenHeight, 1.png
    ox1 := ox1- 100
    oy1 := oy1 - 100
    global wait
    if(wait == false)
    {
        wait := true 
        buf := false

        ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight, pageup.png
        if(ErrorLevel == 0)
        {
            buf := true
            send {PgUp}
        }

        ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight, 2.png
        if(ErrorLevel == 0)
        {
            buf := true
            send {2}
        }

        ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight, 3.png
        if(ErrorLevel == 0)
        {
            buf := true
            send {3}
        }

        ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight, 4.png
        if(ErrorLevel == 0)
        {
            buf := true
            send {4}
        }

        ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight, 5.png
        if(ErrorLevel == 0)
        {
            buf := true
            send {5}
        } 
        wait := false 
        return buf
    }

}
SetTimer,buff,3000

autoAttack(checkbuff)
{
    global manual
    global wait
    if(checkbuff and manual == false)
    {
        if(keybuff())
        {
            return
        }

    }

    if(manual == false && wait)
    {
        return
    }
    CoordMode Pixel
    ImageSearch, ox1, oy1, 0, 0, A_ScreenWidth, A_ScreenHeight, 1.png
    ox1 := ox1- 100
    oy1 := oy1 - 100

    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,*50 e.png
    if(ErrorLevel == 0)
    {
        send {e}
        return
    }

    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight, w.png
    if(ErrorLevel == 0)
    { 
        ImageSearch, ox2, oy2,0, 0, A_ScreenWidth, A_ScreenHeight,*50 4buff.png
        if(ErrorLevel == 0)
        {
            if(manual)
            {
                send wad
            } 
            else{
                send sw
            }
            return
        }
        ImageSearch, ox2, oy2,0, 0, A_ScreenWidth, A_ScreenHeight,*50 qw.png
        if(ErrorLevel == 0)
        {
            if(manual)
            {
                send wad
            } 
            else{
                send sw
            }
            return
        }        
    }

    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight, q.png
    if(ErrorLevel == 0)
    {
        ImageSearch, ox2, oy2,0, 0, A_ScreenWidth, A_ScreenHeight,*50 4buff.png
        if(ErrorLevel == 0)
        {
            if(manual)
            {
                send qad
            } 
            else{
                send sq
            }
            return
        }

        ImageSearch, ox2, oy2,0, 0, A_ScreenWidth, A_ScreenHeight,*50 qw.png
        if(ErrorLevel == 0)
        {
            if(manual)
            {
                send qad
            } 
            else{
                send sq
            }
            return
        }       
    }

    if(manual)
    {
        send da
    }else
    {
        send dr
    }

}

S(){
    Random, rand, 1, 10
    if(rand > 5)
    {
        send n
        sleep 100
        send m
    }else
    {
        send d
        sleep 40
        send s
    }
}



z::autoAttack(true)
F1::SetLeft()
F2::SetRight()
F3::SetHeight()
F4::mover :=-1
F5::mover := 1
Tab::keybuff()
$s::S()
^X::jump := not jump


F6::
manual := !manual
return


F7::
Suspend
manual := true
return

Loop{

}
