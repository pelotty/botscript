
init()
{
	    SetWorkingDir %A_ScriptDir%
        global mleft := -1000
        global mright := -1000
        global mheight := -1000
        global mover := 0
        global state := 0
        global jump := true
        global wait :=false
        global manual := true
        global forcebuff := false
}
init()
Suspend


text(TX) {
        return
                Gui, Destroy
                Gui Color, White
                Gui -caption +toolwindow +AlwaysOnTop
                Gui font, s14 bold, Arial
                Gui add, text,cYellow TransColor, %TX% %A_Now%
                Gui Show, % "x" A_ScreenWidth-400 " y" 0, TRANS-WIN
                WinSet TransColor, White, TRANS-WIN

                WinWait, ahk_class MapleStoryClassSG
                WinRestore, ahk_class MapleStoryClassSG	
                WinActivate, ahk_class MapleSto1ryClassSG             
}

SetLeft()
{
    global mleft
    CoordMode Pixel
    ImageSearch, mleft, soy, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
    if(ErrorLevel == 0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}

SetRight()
{
    global mright
    CoordMode Pixel
    ImageSearch, mright, soy, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
    if(ErrorLevel == 0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}

SetHeight()
{
    global mheight
    CoordMode Pixel
    ImageSearch, sox, mheight, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
    if(ErrorLevel == 0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}

mainmove()
{

    global mleft
    global mright 
    global mheight
    global mover
    global state
    global wait
    global manual
  ; GetKeyState, tabstate, Tab
   ; if (tabstate = "D")
   ;    keybuff()
    if(manual)
    {
        return
    } 
    if(mleft >= 0 and mright >=0)
    {
        ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
        if(ErrorLevel == 0)
        {
            if(px <= mleft and state != -1)
            {
                send {left UP}
                sleep 50
                send {right Down}
                state := -1
            }Else
            if(px >= mright and state != 1)
            {
                send {right UP}
                sleep 50
                send {left Down}
                state := 1
            }

        }
    }
}
SetTimer, mainmove, 30


Rune()
{
    ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight,*50 lock.png
    if(ErrorLevel==0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}
SetTimer,Rune,3000


core()
{
    ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight,*50 core.png
    if(ErrorLevel==0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}
SetTimer,core,3000


bot()
{
    ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight,*50 botcheck.png
    if(ErrorLevel==0)
    {
        SoundPlay, %A_WinDir%\Media\notify.wav
    }
}
SetTimer,bot,3000


attack()
{
    CoordMode Pixel
    ImageSearch, ox1, oy1, 0, 0, A_ScreenWidth, A_ScreenHeight,*50 1.png
    ox1 := ox1- 100
    oy1 := oy1 - 100
    buf := false

    ;ImageSearch, ox, oy,0, 0, A_ScreenWidth, A_ScreenHeight,*80 backform.png
    ;if(ErrorLevel == 0)
    {
        ;ImageSearch, ox, oy,0, 0, A_ScreenWidth, A_ScreenHeight,*80 backform2.png
        ;if(ErrorLevel == 0)
        {
               ; buf := true
                ;text("ins")
                ;send {Insert} 
               ; return true
        }
    }   

    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,q.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("q")
            send {q} 
            return true
    }   

    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,*50 pageup.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("pgup")
            send {PgUp} 
            return true
    }   
   
    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,*80 home.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("home")
            send {home} 
            return true
      
    }   
      
    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,r.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("r")
            send {r}  
            return true
    }   
      


    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,2.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("2")
            send {2}    
            return true
    }   


    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,3.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("3")
            send {3} 
            return true
    }   


    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,4.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("4")
            send {4}    
            return true
    }   

    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,5.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("5")
            send {5}    
            return true 
    }   


    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,c3.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("c3")
            send {c}
            return true
    }   

    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,c4.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("c4")
            send {c}
            return true
    }   


    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,*80 w.png
    if(ErrorLevel == 0)
    {
            buf := true
            text("w")
            send {w}
    }
    return buf
}

NormalBuff()
{

    global wait
    global manual
    if(manual)
    {
        return
    }  
    if(wait == false)
    {
        wait := true
        send {1}
        sleep 1000
        send {Ctrl}
        wait := false
    }
}
SetTimer,NormalBuff,30000
NormalBuff()


attackmove(pass=true)
{
        global jump
        global wait
        global mover
        global state    
        global mheight
        global ebufact
        global manual



    GetKeyState, tabstate, Tab
    if(manual and pass)
    {
        return
    }        
 

    if(manual == false)
    {
        ImageSearch, px, py, 0, 0, A_ScreenWidth, A_ScreenHeight, player.png
        if(ErrorLevel == 0)
        {
            if(state == mover)
            {
                if(py > mheight)
                {
                    wait := true
                    send {Del Down}
                    sleep 300
                    send {Del UP}
                    wait := false
                    return
                }
            }
        }

        if(ebufact == false)
        {
            sleep 50
            send {a}
        }
	
    }



    CoordMode Pixel
    ImageSearch, ox1, oy1, 0, 0, A_ScreenWidth, A_ScreenHeight,1.png
    if(ErrorLevel == 0)
    {
	ox1 := ox1- 100
    	oy1 := oy1 - 100
    }else
    {
     ox1 := 0
     oy1 := 0
    }



    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth,A_ScreenHeight,q.png
    if(ErrorLevel != 0)
    {
        buf := true
        send {q}
        return
    }


    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth,A_ScreenHeight,e.png
    if(ErrorLevel != 0)
    {
        buf := true
        send {e}
        return
    }

    if(manual == false)
    {
        ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,s.png
        if(ErrorLevel != 0)
        {
            buf := true
            send {s}
            return
        }
    }    
    
    if(manual == true)
    {
        ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,r.png
        if(ErrorLevel != 0)
        {
            buf := true
            send {r}
            return
        }
    }     


    ImageSearch, ox, oy,ox1, oy1, A_ScreenWidth, A_ScreenHeight,w.png
    if(ErrorLevel != 0)
    {
        buf := true
        send {w}
        return
    }
}
SetTimer,attackmove,600


jump()
{
    global manual
    if(manual == false)
    {
        send {d}
    }
}


F1::SetLeft()
F2::SetRight()
F3::SetHeight()
F4::mover :=-1
F5::mover := 1
z::AttackMove(false)
F6::
manual := !manual
return

F8::text(A_Now)
F7::
Suspend
manual := true
return

Loop{

}